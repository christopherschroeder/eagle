# How to draft a new release of eagle

Drafting a new release of eagle includes creating a new tag in git for
manual installation, updating the package on PyPI and updating the package
on Bioconda.

## Step 0 - Gather dependencies and clean up code

Befor drafting a new release, you should make sure, that the code you are 
releasing works and conforms to PEP8. Tools like 
[flakes8](http://flake8.pycqa.org/en/latest/) should be run the projects root 
directory.

Additionally you should know all dependencies you included since the last 
release.


## Step 1 - Updating setup.py

Following [PEP440](https://www.python.org/dev/peps/pep-0440) you should choose
a new version number (We will use X.Y.Z as the chosen version number). 
Include the version number along with the new dependencies in the setup.py 
located in your projects root directory.

Commit these changes, create a tag and push the commit

    git add setup.py
    git commit -m "Updated version to X.Y.Z"
    git tag -a vX.Y.Z -m "Version X.Y.Z"
    git push

### (Optional) Step 1.1 - Test installation

Try installing the created tag in a fresh environment and test it.

    pip install git+https://bitbucket.org/christopherschroeder/eagle.git@vX.Y.Z

## (Optional) Step 2 - Submit to PyPI

If not already done, create a ~/pypirc configuration file like this:

    [distutils]
    index-servers =
      pypi

    [pypi]
    repository=https://pypi.python.org/pypi
    username=your_username
    password=your_password

Next create a source distribution for eagle

    python setup.py sdist

Since we do not use any compiled extensions, we can create a "Pure Python 
Wheel" 

    python setup.py bdist_wheel

Lastly we can upload both to PyPI via twine. 

    twine upload dist/*

**Note:** Older guides reference `setup.py upload` but this is discouraged 
due to security reasons. This method sends your password in plaintext. If you 
do not have twine installed, it is available from PyPI 

    pip install twine

## Step 3 - Submit to Bioconda

The main distribution of eagle is through 
[Bioconda](http://bioconda.github.io/).
If you already have access to the Bioconda repositry and have cloned and forked
their repository, you can skip 3.1 and 3.2. Be sure to update your fork, if you
skipped 3.2.

### Step 3.1 - Get access to the Bioconda repository

Ask [here](https://github.com/bioconda/recipes/issues/1) for beeing included as
a contributor to Bioconda. Simply asking should be enough. You need a 
[GitHub](https://github.com/) account for that.

### Step 3.2 - Fork the recipes project

You will work on a fork of the recipes project and make a pull request, once
you have updated your recipe. Click on **Fork** on the 
[recipes](https://github.com/bioconda/bioconda-recipes) project and clone it
locally.

    git@github.com:<yourgithubusername>/bioconda-recipes.git

Add bioconda as an upstream remote to your fork.

    git remote add upstream https://github.com/bioconda/bioconda-recipes.git

### Step 3.3 - Software preperations

_If you have already done this, you can safely skip this._

Install the latest version of docker and conda. You should install docker via
your distribution package manager. In most distibutions you have to be a member
of the docker group. You can install conda via 
[miniconda](http://conda.pydata.org/docs/install/quick.html) be sure to install
the Python 3 version of conda.

### Step 3.4 - Update recipes project

Update your fork to include the current version of the bioconda recipes

    git checkout master
    git pull upstream master

To simulated similar conditions to the build server, your conda channels must
be rearranged. The `simulate-travis.py` script will to this. It will also add
any missing channels for bioconda to your `~/.condarc`

    ./simulate-travis.py --set-channel-order

Next we have to install software to mimic the build server

    ./simulate-travis.py --install-requirements

This will install all software we need running the build tests.

### Step 3.5 - Update the eagle recipe

The eagle recipe is located in `recipes/eagle/`. There are two files present,
`build.sh` and `meta.sh`. The `build.sh` most probably does not need to be 
changed. It contains all commands to build and install eagle, which is just
`python setup.py install`.

Open the `meta.yaml` file and update the version and dependencies. Most 
probably you have to update `version`, `fn`, `url` and `md5`. To calculate
the updated md5 value, download your new tag and call md5sum

    wget -q -O - https://bitbucket.org/christopherschroeder/eagle/get/vX.Y.Z.tar.gz | md5sum

### Step 3.6 Test the recipe

Here we test the recipe locally. The `simulate-travis.py` script will create a
docker container and try to build eagle.

    ./simulate-travis.py --docker --package eagle

If the output contains something like:

    INFO bioconda_utils.build:build_recipes(376): BIOCONDA BUILD SUMMARY: successfully built 1 of 1 recipes

you can commit all your changes and push it to your forked project. 

### Step 3.7 Create a pull request

Just create a pull request with the main bioconda repository. Travis will
check your changes. If it failes, try finding out what went wrong and fix it
otherwise, your changes should be merged pretty soon, and your updated
package will be available via Bioconda
